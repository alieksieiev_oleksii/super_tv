package com.devapsforyou.sharatv.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.devapsforyou.sharatv.utils.Utils;
import com.devapsforyou.sharatv.service.BackgroundService;

public class BootBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Utils.getInstance().printToLogs("BootBroadcastReceiver onReceive");
        context.startService(new Intent(context, BackgroundService.class));
    }
}
