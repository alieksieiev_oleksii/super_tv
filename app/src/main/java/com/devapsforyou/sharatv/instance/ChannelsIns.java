package com.devapsforyou.sharatv.instance;

import com.devapsforyou.sharatv.models.ItemChannel;

import java.util.ArrayList;

public class ChannelsIns {

    private static ChannelsIns instance;
    private ArrayList<ItemChannel> channelBeansArray;

    private ChannelsIns() {
        channelBeansArray = new ArrayList<ItemChannel>();
    }

    public static void reloadChannelWorker(){
        if(instance != null) {
            instance = null;
            instance = new ChannelsIns();
        }
    }

    public static ChannelsIns getInstance() {
        if (instance == null) {
            instance = new ChannelsIns();
        }
        return instance;
    }

    public void addChannelBean(String channelName, String channelLink, String channelImageLink, String showName) {
        channelBeansArray.add(new ItemChannel(channelName, channelLink, channelImageLink, showName));
    }

    public ItemChannel getChannelBean(int position) {
        return channelBeansArray.get(position);
    }

    public int getCountChannelsBeans() {
        return channelBeansArray.size();
    }

}
