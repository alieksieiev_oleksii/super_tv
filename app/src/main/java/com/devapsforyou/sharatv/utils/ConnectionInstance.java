package com.devapsforyou.sharatv.utils;

import com.devapsforyou.sharatv.tcpconnection.TCPConnection;

public class ConnectionInstance {

    private static ConnectionInstance connectionInstance;
    private static TCPConnection tcpConnection;

    public static ConnectionInstance getInstance(){
        if(connectionInstance == null) {
            return new ConnectionInstance();
        }
        return connectionInstance;
    }

    public TCPConnection getTcpConnection(){
        return tcpConnection;
    }

}
