package com.devapsforyou.sharatv.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import static android.content.Context.MODE_PRIVATE;

public class Utils {

    private static Utils utils;
    private final String LOG_TAG = "log";
    private final boolean printLogs = true;
    public EmumAppState emumAppState;
    private SharedPreferences sharedPreferences;
    public boolean blnAppIsRun = false;

    public static Utils getInstance(){
        if(utils == null){
            return new Utils();
        }
        return utils;
    }

    public synchronized void printToLogs(String string) {
        if(printLogs && (string != null))Log.d(LOG_TAG, string);
    }

    public boolean checkNetworkConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public synchronized void showToast(Context context, String message){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public synchronized String getPrintStackTrace(Exception e){
        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        e.printStackTrace(printWriter);
        return writer.toString();
    }

    public int getOrienatation(Context context){
        if(context.getApplicationContext().getResources().getConfiguration().orientation == 1) {
            return 2;
        }
        else if(context.getApplicationContext().getResources().getConfiguration().orientation == 2){
            return 3;
        }
        else {
            return 1;
        }
    }

    public boolean isInteger( String input ) {
        try {
            Integer.parseInt( input );
            return true;
        }
        catch( Exception e ) {
            return false;
        }
    }

    public void savePreferencesString(Context context, String file_key, String settings_key, String value) {
        sharedPreferences = context.getApplicationContext().getSharedPreferences(file_key, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(settings_key, value);
        printToLogs("Utils | savePreferencesString " + value);
        editor.commit();
    }

    public void savePreferencesInt(Context context, String file_key, String settings_key, int value) {
        sharedPreferences = context.getApplicationContext().getSharedPreferences(file_key, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(settings_key, value);
        printToLogs("Utils | savePreferencesInt " + String.valueOf(value));
        editor.commit();
    }

    public String loadPreferencesString(Context context,  String file_key, String settings_key) {
        sharedPreferences = context.getApplicationContext().getSharedPreferences(file_key, MODE_PRIVATE);
        printToLogs("Utils | loadPreferencesString " + sharedPreferences.getString(settings_key, "freetv.com.ua"));
        return sharedPreferences.getString(settings_key, "freetv.com.ua");
    }

    public int loadPreferencesInt(Context context,  String file_key, String settings_key) {
        sharedPreferences = context.getApplicationContext().getSharedPreferences(file_key, MODE_PRIVATE);
        printToLogs("Utils | loadPreferenceInt " + String.valueOf(sharedPreferences.getInt(settings_key, 8189)));
        return sharedPreferences.getInt(settings_key, 8189);
    }
}