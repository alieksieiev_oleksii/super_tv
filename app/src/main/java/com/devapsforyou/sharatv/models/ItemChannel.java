package com.devapsforyou.sharatv.models;

public class ItemChannel {

    private String title;
    private String link;
    private String imageLink;
    private String showName;

    public ItemChannel(String title, String link, String imageLink, String showName) {
        this.title = title;
        this.link = link;
        this.imageLink = imageLink;
        this.showName = showName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String note) {
        this.link = link;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getShowName() {
        return this.showName;
    }

    public void setShowName(String showName){
        this.showName = showName;
    }


}