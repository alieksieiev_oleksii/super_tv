package com.devapsforyou.sharatv.fragments;

import com.devapsforyou.sharatv.utils.Utils;
import com.devapsforyou.sharatv.R;

public class AllFragment extends NotesListFragment {

    public static AllFragment newInstance() {
        return new AllFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_all;
    }

    @Override
    protected int getNumColumns() {
        return Utils.getInstance().getOrienatation(getContext());
    }

    @Override
    protected int getNumItems() {
        return 10;
    }
}