package com.devapsforyou.sharatv.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.app.NotificationCompat;


import com.devapsforyou.sharatv.R;
import com.devapsforyou.sharatv.activity.AdvActivity;
import com.devapsforyou.sharatv.models.AdvJsonObj;
import com.devapsforyou.sharatv.tcpconnection.TCPConnection;
import com.devapsforyou.sharatv.tcpconnection.TCPConnectionListener;
import com.devapsforyou.sharatv.utils.Utils;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class BackgroundService extends Service {

    private TCPConnection connection;
    private MyAsyncThread myAsyncThread;
    private AdvJsonObj advJsonObj;
    private Timer timer = new Timer();
    private boolean isConnected = false;
    private boolean isTimerRunning = false;

    @Override
    public void onCreate() {
        super.onCreate();
        Utils.getInstance().printToLogs("Background service | onCreate");
        myAsyncThread = new MyAsyncThread();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Utils.getInstance().printToLogs("Background service | onBind");
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Utils.getInstance().printToLogs("Background service | onStartCommand");
        if (!isConnected){
            if(connection != null)connection.disconnect();
            myAsyncThread.cancel(true);
            myAsyncThread = new MyAsyncThread();
            myAsyncThread.execute();
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(connection != null) connection.disconnect();
        Utils.getInstance().printToLogs("Background service | onDestroy");
    }

    class MyAsyncThread extends AsyncTask<Void, Void, String> implements TCPConnectionListener {

        @Override
        protected String doInBackground(Void... params) {
            try {
                connection = new TCPConnection(
                        this,
                        Utils.getInstance().loadPreferencesString(getApplicationContext(), getString(R.string.str_shar_pref_key), getString(R.string.str_socket_server_path)),
                        Utils.getInstance().loadPreferencesInt(getApplicationContext(), getString(R.string.str_shar_pref_key), getString(R.string.str_socket_server_port)));
            }catch (IOException e){
                Utils.getInstance().printToLogs("Background service | Error at connect to server");
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        public void onReceiveString(TCPConnection tcpConnection, String value) {
            Utils.getInstance().printToLogs(value);
            if(value != null && value.equals("ping from server") && (connection != null)){
                connection.sendString("online!");
                return;
            }
            if(value != null && Utils.getInstance().isInteger(value)) return;
            if(value != null){
                advJsonObj = new AdvJsonObj(value);
                if(advJsonObj.getBlnShowAdv() && !Utils.getInstance().blnAppIsRun){
                    Utils.getInstance().printToLogs("Background service | Show adv");
                    Intent intent = new Intent(getApplicationContext(), AdvActivity.class);
                    intent.putExtra("adv_type", advJsonObj.getAdvType());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                if(advJsonObj.getBlnShowNotif() && !Utils.getInstance().blnAppIsRun){
                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(advJsonObj.getNotifTitle())
                            .setDefaults(Notification.DEFAULT_ALL)
                            .setAutoCancel(true)
                            .setContentText(advJsonObj.getNotifContent());
                    Intent resultIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(advJsonObj.getLink()));
                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent =
                            stackBuilder.getPendingIntent(
                                    0,
                                    PendingIntent.FLAG_UPDATE_CURRENT
                            );
                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotificationManager.notify(1, mBuilder.build());
                }
            }

        }

        @Override
        public void onConnectionReady(TCPConnection tcpConnection) {
            isConnected = true;
            Utils.getInstance().printToLogs("Background service | onConnectionReady");
            Utils.getInstance().printToLogs("Background service | Starting timer ");

            if (!isTimerRunning) timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    isTimerRunning = true;
                    try {
                        if (isConnected){
                            Utils.getInstance().printToLogs("Background service | Send ping");
                            connection.sendString("ping server from android");
                        }else {
                            Utils.getInstance().printToLogs("Background service | Try connect......");
                            if (Utils.getInstance().checkNetworkConnection(getApplicationContext())){
                                myAsyncThread.cancel(true);
                                myAsyncThread = new MyAsyncThread();
                                myAsyncThread.execute();
                            }else {
                                Utils.getInstance().printToLogs("Background service | No internet connection");
                            }
                        }

                    }catch (Exception e){
                        Utils.getInstance().printToLogs("Background service | IllegalStateException");
                        Utils.getInstance().printToLogs(Utils.getInstance().getPrintStackTrace(e));
                    }
                }
            }, 0, 3 * 1000);
        }

        @Override
        public void onException(TCPConnection tcpConnection, Exception e) {
            isConnected = false;
            Utils.getInstance().printToLogs("Background service | onException");
        }

        @Override
        public void onDisconnect(TCPConnection tcpConnection) {
            isConnected  = false;
            Utils.getInstance().printToLogs("Background service | onDisconnect");
        }
    }
}
