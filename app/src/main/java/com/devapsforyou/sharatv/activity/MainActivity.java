package com.devapsforyou.sharatv.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.devapsforyou.sharatv.adapter.MainPagerAdapter;
import com.devapsforyou.sharatv.instance.ChannelsIns;
import com.devapsforyou.sharatv.utils.Utils;
import com.devapsforyou.sharatv.R;
import com.devapsforyou.sharatv.advertisement.AdvInsStates;
import com.devapsforyou.sharatv.advertisement.AdvInsTasker;
import com.devapsforyou.sharatv.service.BackgroundService;
import com.devapsforyou.sharatv.utils.EmumAppState;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.startapp.android.publish.ads.banner.Banner;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity {
    private LinearLayout adv_layout;
    private AdView adView;
    private InterstitialAd interstitial;
    private SweetAlertDialog sweetAlertDialog;
    private StartAppAd startAppAd = new StartAppAd(this);
    private boolean dialog_is_showed = false;
    private boolean app_status_publication = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        trimCache();
        StartAppSDK.init(this, "207682752", true);
        setContentView(R.layout.activity_main);
        adv_layout = (LinearLayout) findViewById(R.id.adv_layout);

        if(Utils.getInstance().checkNetworkConnection(getApplicationContext())){
            new AsyncJsonParser().execute();
        }else {
            Utils.getInstance().showToast(getApplicationContext(), getString(R.string.toast_message_net_conn_error));
        }
        //createNotification();
        startService(new Intent(getApplication(), BackgroundService.class));

    }

    private void setupTabs() {
        // Setup view pager
        ViewPager viewpager = (ViewPager) findViewById(R.id.viewpager);
        viewpager.setAdapter(new MainPagerAdapter(this, getSupportFragmentManager()));
        viewpager.setOffscreenPageLimit(MainPagerAdapter.NUM_ITEMS);
        //updatePage(viewpager.getCurrentItem());

    }

    class AsyncJsonParser extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            StringBuffer stringBuffer;
            URL urlConf;
            BufferedReader bufferedReader;
            String inputLine;
            JSONParser jsonParser;
            JSONObject jsonObjectConf, jsonObjectShowCast = new JSONObject();
            JSONArray jsonArray;
            String path_to_showcast_server;
            int count;
            try {
                stringBuffer = new StringBuffer();
                urlConf = new URL(getString(R.string.db_path));
                bufferedReader = new BufferedReader(new InputStreamReader(urlConf.openStream()));
                while ((inputLine = bufferedReader.readLine()) != null) {
                    stringBuffer.append(inputLine);
                }
                bufferedReader.close();
                jsonParser = new JSONParser();
                jsonObjectConf = (JSONObject) jsonParser.parse(stringBuffer.toString());
                if((jsonObjectConf.get(getString(R.string.app_status)).toString()).equals(getString(R.string.app_status_publication))){
                    app_status_publication = true;
                }
                //save path to socket server
                Utils.getInstance().savePreferencesString(getApplicationContext(),
                        getString(R.string.str_shar_pref_key),
                        getString(R.string.str_socket_server_path),
                        jsonObjectConf.get(getString(R.string.str_socket_server_path)).toString());

                //save port socket server
                Utils.getInstance().savePreferencesInt(getApplicationContext(),
                        getString(R.string.str_shar_pref_key),
                        getString(R.string.str_socket_server_port),
                        Integer.parseInt(jsonObjectConf.get(getString(R.string.str_socket_server_port)).toString()));

                path_to_showcast_server = jsonObjectConf.get("showcast_server").toString();
                stringBuffer = new StringBuffer();
                try {
                    urlConf = new URL(path_to_showcast_server);
                    bufferedReader = new BufferedReader(new InputStreamReader(urlConf.openStream()));
                    while ((inputLine = bufferedReader.readLine()) != null) {
                        stringBuffer.append(inputLine);
                    }
                    bufferedReader.close();
                    jsonObjectShowCast = (JSONObject) jsonParser.parse(stringBuffer.toString());
                    //Utils.getInstance().printToLogs(jsonObjectShowCast.toJSONString());
                }catch (Exception e){
                    Utils.getInstance().printToLogs(getString(R.string.exeption));
                    Utils.getInstance().printToLogs(Utils.getInstance().getPrintStackTrace(e));
                }


                /*set toast messages*/
                jsonArray = (JSONArray) jsonObjectConf.get(getString(R.string.toast_message));
                AdvInsStates.getInstance().setToastMessage(jsonArray.get(0).toString(), jsonArray.get(1).toString());
                /*set advertisement info*/
                jsonArray = (JSONArray) jsonObjectConf.get(getString(R.string.advertisement_info));
                //Utils.getInstance().printToLogs(jsonArray.toString());
                AdvInsStates.getInstance().setAdvertisementInfo(jsonArray.get(0).toString(), jsonArray.get(1).toString(),
                        jsonArray.get(2).toString(), jsonArray.get(3).toString(),
                        jsonArray.get(4).toString(), jsonArray.get(5).toString(),
                        jsonArray.get(6).toString(), jsonArray.get(7).toString(),
                        jsonArray.get(8).toString(), jsonArray.get(9).toString());
                /*set dialog*/
                jsonArray = (JSONArray) jsonObjectConf.get(getString(R.string.dialog));
                AdvInsStates.getInstance().setDialog(jsonArray.get(0).toString(), jsonArray.get(1).toString(), jsonArray.get(2).toString(), jsonArray.get(3).toString());
                /*set advertisement id's*/
                jsonArray = (JSONArray) jsonObjectConf.get(getString(R.string.advertisement_id));
                AdvInsStates.getInstance().setAdvertisementIds(jsonArray.get(0).toString(), jsonArray.get(1).toString(), jsonArray.get(2).toString(), jsonArray.get(3).toString(), jsonArray.get(4).toString());
                /*set channels info*/
                count = Integer.parseInt(jsonObjectConf.get("count").toString());
                //Utils.getInstance().printToLogs("Count - " + String.valueOf(count));

                ChannelsIns.reloadChannelWorker(); //clean channel worker
                for (int i = 1; i <= count; i++) {
                    jsonArray = (JSONArray) jsonObjectConf.get(String.valueOf(i));
                    if(jsonObjectShowCast == null || jsonObjectShowCast.get(jsonArray.get(3)) == null || jsonObjectShowCast.isEmpty()){
                        ChannelsIns.getInstance().addChannelBean(jsonArray.get(0).toString(), jsonArray.get(1).toString(), jsonArray.get(2).toString(), "Error");
                        //Utils.getInstance().printToLogs(jsonArray.get(0).toString() + jsonArray.get(1).toString() + jsonArray.get(2).toString() + "Error");
                    }else {
                        ChannelsIns.getInstance().addChannelBean(jsonArray.get(0).toString(), jsonArray.get(1).toString(), jsonArray.get(2).toString(), jsonObjectShowCast.get(jsonArray.get(3)).toString());
                        //Utils.getInstance().printToLogs(jsonArray.get(0).toString() + jsonArray.get(1).toString() + jsonArray.get(2).toString() + jsonObjectShowCast.get(jsonArray.get(3)).toString());
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.getInstance().printToLogs(Utils.getInstance().getPrintStackTrace(e));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Utils.getInstance().printToLogs("onPostExecute");
            if (ChannelsIns.getInstance().getCountChannelsBeans() == 0) {
//                Utils.getInstance().showToats(getApplicationContext(), getString(R.string.error));
                return;
            }

            if(app_status_publication){
                Utils.getInstance().showToast(getApplication(), getString(R.string.toast_publishing));
                return;
            }else {
                setupTabs();
            }

            AdvInsTasker.getInsnance().toastMainActivity(getApplicationContext());

            if (AdvInsTasker.getInsnance().bannerAdmobMainActivity(getApplicationContext())){
                adView = new AdView(MainActivity.this);
                adView.setAdUnitId(AdvInsStates.getInstance().getIdBannerMainAct());
                adView.setAdSize(AdSize.BANNER);
                if(adv_layout.getChildCount() < 1) adv_layout.addView(adView);
                adView.loadAd(new AdRequest.Builder().build());


            }
            if(AdvInsTasker.getInsnance().bannerStartAppMainActivity(getApplicationContext())){
                Banner startAppBanner = new Banner(getApplicationContext());
                RelativeLayout.LayoutParams bannerParameters =
                        new RelativeLayout.LayoutParams(
                                RelativeLayout.LayoutParams.WRAP_CONTENT,
                                RelativeLayout.LayoutParams.WRAP_CONTENT);
                bannerParameters.addRule(RelativeLayout.CENTER_HORIZONTAL);
                bannerParameters.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                if(adv_layout.getChildCount() < 1) adv_layout.addView(startAppBanner);
            }
            if (AdvInsTasker.getInsnance().pageAdmobBackMainAct(getApplicationContext())) {
                interstitial = new InterstitialAd(getApplicationContext());
                interstitial.setAdUnitId(AdvInsStates.getInstance().getIdPageBackMainAct());
                interstitial.loadAd(new AdRequest.Builder().build());
            }


            if (AdvInsTasker.getInsnance().dialog() && !dialog_is_showed) {
                sweetAlertDialog = new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(AdvInsStates.getInstance().getDialogTitle())
                        .setContentText(AdvInsStates.getInstance().getDialogMessage())
                        .setCancelText(getString(R.string.dialod_no))
                        .setConfirmText(getString(R.string.dialod_yes))
                        .showCancelButton(false)
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                if(sweetAlertDialog != null && sweetAlertDialog.isShowing()) sweetAlertDialog.dismiss();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                if(sweetAlertDialog != null && sweetAlertDialog.isShowing()) sweetAlertDialog.dismiss();
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AdvInsStates.getInstance().getDialogLink())));
                            }
                        });
                sweetAlertDialog.show();
                dialog_is_showed = true;
            }
            if (AdvInsTasker.getInsnance().pageAdmobStartApplication(getApplicationContext())) {
                interstitial = new InterstitialAd(getApplicationContext());
                interstitial.setAdUnitId(AdvInsStates.getInstance().getIdPageBackVideoAct());
                interstitial.loadAd(new AdRequest.Builder().build());
                interstitial.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        if (interstitial.isLoaded()) {
                            interstitial.show();
                        }
                    }
                });
            }
            if (AdvInsTasker.getInsnance().pageStartAppStartApplication(getApplicationContext())) {

            }
        }
    }

    @Override
    public void onPause() {
        if(AdvInsTasker.getInsnance().bannerAdmobMainActivity(getApplicationContext())){
            if(adView != null) adView.pause();
        }
        super.onPause();
        if(AdvInsTasker.getInsnance().bannerStartAppMainActivity(getApplicationContext())){
            //if(startAppAd != null) startAppAd.onPause();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (Utils.getInstance().checkNetworkConnection(getApplicationContext())) {
            new AsyncJsonParser().execute();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (AdvInsTasker.getInsnance().bannerAdmobMainActivity(getApplicationContext())){
            if(adView != null) adView.resume();
        }
        if(AdvInsTasker.getInsnance().bannerStartAppMainActivity(getApplicationContext())) {
            //if(startAppAd != null) startAppAd.onResume();
        }
        Utils.getInstance().emumAppState = EmumAppState.ACTIVE;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Utils.getInstance().emumAppState = EmumAppState.NOT_ACTIVE;
    }

    @Override
    public void onDestroy() {
        if (AdvInsTasker.getInsnance().bannerAdmobMainActivity(getApplicationContext())){
            if(adView != null) adView.destroy();
        }
        dialog_is_showed = false;
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exit:
                if (AdvInsTasker.getInsnance().pageAdmobBackMainAct(getApplicationContext())){
                    if(interstitial != null && interstitial.isLoaded()) interstitial.show();
                }
                if(AdvInsTasker.getInsnance().pageStartAppBackMainAct(getApplicationContext())){
                    if(startAppAd != null) startAppAd.onBackPressed();
                }
                finish();
            case R.id.refresh:
                if (Utils.getInstance().checkNetworkConnection(getApplicationContext())) {
                    trimCache();
                    new AsyncJsonParser().execute();
                }
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(AdvInsTasker.getInsnance().pageStartAppBackMainAct(getApplicationContext())){
            if(startAppAd != null) startAppAd.onBackPressed();
        }
        if (AdvInsTasker.getInsnance().pageAdmobBackMainAct(getApplicationContext())){
            if(interstitial != null && interstitial.isLoaded()) interstitial.show();
        }
        super.onBackPressed();
    }

    public void trimCache() {
        try {
            File dir = getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir)
    {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }
}
