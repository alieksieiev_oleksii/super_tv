package com.devapsforyou.sharatv.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.devapsforyou.sharatv.advertisement.AdvInsStates;
import com.devapsforyou.sharatv.utils.Utils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;

public class AdvActivity extends Activity{

    private String adv_type;
    private InterstitialAd interstitial;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adv_type = getIntent().getStringExtra("adv_type");
        if (adv_type.equals("start_app")){
            StartAppSDK.init(this, "207682752", true);
            StartAppAd.showAd(this);
            finish();
        }else if(adv_type.equals("admob")){
            Utils.getInstance().printToLogs("AdvActivity - show admob adv");
            interstitial = new InterstitialAd(getApplicationContext());
            interstitial.setAdUnitId(AdvInsStates.getInstance().getIdPageBackVideoAct());
            interstitial.loadAd(new AdRequest.Builder().build());
            interstitial.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    if (interstitial.isLoaded()) {
                        interstitial.show();
                        finish();
                    }
                }
            });
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //if(interstitial != null && interstitial.isLoaded()) interstitial.show();
        finish();
    }
}
