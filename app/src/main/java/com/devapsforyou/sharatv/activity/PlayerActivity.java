package com.devapsforyou.sharatv.activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.devapsforyou.sharatv.utils.Utils;
import com.devbrackets.android.exomedia.core.video.scale.ScaleType;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.EMVideoView;
import com.devapsforyou.sharatv.R;
import com.devapsforyou.sharatv.advertisement.AdvInsStates;
import com.devapsforyou.sharatv.advertisement.AdvInsTasker;
import com.devapsforyou.sharatv.instance.ChannelsIns;
import com.devapsforyou.sharatv.utils.EmumAppState;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.startapp.android.publish.ads.banner.Banner;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppSDK;

public class PlayerActivity extends Activity implements OnPreparedListener {

    private RelativeLayout mainLayout;
    private int position;
    private StartAppAd startAppAd = new StartAppAd(this);
    private Banner startAppBanner;
    private AdView adView;
    private InterstitialAd interstitial;
    private EMVideoView videoView;
    protected boolean pausedInOnStop = false;
    private Button button;
    private int videoLayout;
    private int curBright = 0, curVol = 0;
    private AudioManager audioManager;
    private SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        StartAppSDK.init(this, "207682752", true);
        setContentView(R.layout.activity_video);
        findViewSByID();

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        curBright = getScreenBrightness();
        curVol = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        videoView.setOnPreparedListener(this);
        AdvInsTasker.getInsnance().toastVideoActivity(getApplicationContext());
        position = getIntent().getIntExtra(getString(R.string.intent), 1);
        videoView.setVideoURI(Uri.parse(ChannelsIns.getInstance().getChannelBean(position).getLink()));

        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(button.getVisibility() == View.VISIBLE) {
                    hideUI();
                }
                else {
                    showUI();
                }
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoLayout++;
                if (videoLayout == 5) {
                    videoLayout = 0;
                }
                switch (videoLayout) {
                    case 0:
                        videoView.setScaleType(ScaleType.CENTER_INSIDE);
                        break;
                    case 1:
                        videoView.setScaleType(ScaleType.CENTER);
                        break;
                    case 2:
                        videoView.setScaleType(ScaleType.CENTER_CROP);
                        break;
                    case 3:
                        videoView.setScaleType(ScaleType.FIT_CENTER);
                        break;
                    case 4:
                        videoView.setScaleType(ScaleType.NONE);
                        break;
                }
                savePreferences();
            }
        });


        if(AdvInsTasker.getInsnance().bannerAdmobVideoActivity(getApplicationContext())){
            adView = new AdView(PlayerActivity.this);
            adView.setAdUnitId(AdvInsStates.getInstance().getIdBannerVideoAct());
            adView.setAdSize(AdSize.BANNER);
            mainLayout.addView(adView);
            adView.loadAd(new AdRequest.Builder().build());
        }

        if(AdvInsTasker.getInsnance().bannerStartAppVideoActivity(getApplicationContext())){
            startAppBanner = new Banner(getApplicationContext());
            RelativeLayout.LayoutParams bannerParameters =
                    new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
            bannerParameters.addRule(RelativeLayout.CENTER_HORIZONTAL);
            bannerParameters.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            mainLayout.addView(startAppBanner, bannerParameters);
        }

        if (AdvInsTasker.getInsnance().pageAdmobBackVideoAct(getApplicationContext())) {
            interstitial = new InterstitialAd(getApplicationContext());
            interstitial.setAdUnitId(AdvInsStates.getInstance().getIdPageBackVideoAct());
            interstitial.loadAd(new AdRequest.Builder().build());
        }
    }

    private void findViewSByID(){
        videoView = (EMVideoView) findViewById(R.id.video_view);
        button = (Button) findViewById(R.id.button);
        mainLayout = (RelativeLayout) findViewById(R.id.relative_layout);
    }

    private int getScreenBrightness(){
        try {
            return Settings.System.getInt(getApplicationContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 0);
        }catch (Exception e){
            Utils.getInstance().showToast(getApplicationContext(), getString(R.string.exeption));
        }
        return 0;
    }

    private void setScreenBrightness(int brightnessValue){
        try {
            if(brightnessValue >= 0 && brightnessValue <= 255){
                //Settings.System.putInt(getApplicationContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, brightnessValue);
            }
        }catch (Exception e){
            Utils.getInstance().showToast(getApplicationContext(), getString(R.string.exeption));
        }
    }

    @Override
    public void onPrepared() {
        loadPreferences();
        videoView.start();
        hideUI();
        Utils.getInstance().blnAppIsRun = true;
    }

    private void hideUI() {
        if (Build.VERSION.SDK_INT == 14 || Build.VERSION.SDK_INT == 15) {
            if (AdvInsTasker.getInsnance().bannerAdmobVideoActivity(getApplicationContext()) && adView != null) adView.setVisibility(View.INVISIBLE);
            else if (AdvInsTasker.getInsnance().bannerStartAppVideoActivity(getApplicationContext()) && startAppAd != null) startAppBanner.setVisibility(View.INVISIBLE);
            mainLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
            button.setVisibility(View.INVISIBLE);
        } else if (Build.VERSION.SDK_INT >= 16) {
            if (AdvInsTasker.getInsnance().bannerAdmobVideoActivity(getApplicationContext()) && adView != null) adView.setVisibility(View.INVISIBLE);
            else if (AdvInsTasker.getInsnance().bannerStartAppVideoActivity(getApplicationContext()) && startAppBanner != null) startAppBanner.setVisibility(View.INVISIBLE);
            mainLayout.setSystemUiVisibility( View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN);
            button.setVisibility(View.INVISIBLE);
        }
    }

    private void showUI() {
        if (AdvInsTasker.getInsnance().bannerAdmobVideoActivity(getApplicationContext()) && adView != null) adView.setVisibility(View.VISIBLE);
        else if (AdvInsTasker.getInsnance().bannerStartAppVideoActivity(getApplicationContext()) && startAppAd != null) startAppBanner.setVisibility(View.VISIBLE);
        mainLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        button.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause() {
        if(AdvInsTasker.getInsnance().bannerAdmobVideoActivity(getApplicationContext())){
            if(adView != null) adView.pause();
        }
        super.onPause();
        if(AdvInsTasker.getInsnance().bannerStartAppVideoActivity(getApplicationContext())){
            if(startAppAd != null) startAppAd.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AdvInsTasker.getInsnance().bannerAdmobVideoActivity(getApplicationContext())){
            if(adView != null) adView.resume();
        }
        if(AdvInsTasker.getInsnance().bannerStartAppVideoActivity(getApplicationContext())) {
            //if(startAppAd != null) startAppAd.onResume();
        }
        Utils.getInstance().emumAppState = EmumAppState.ACTIVE;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (videoView.isPlaying()) {
            pausedInOnStop = true;
            videoView.pause();
        }
        try {
            setScreenBrightness(curBright);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, curVol, 0);
        }catch (Exception e){
            Utils.getInstance().showToast(getApplicationContext(), getString(R.string.exeption));
        }
        Utils.getInstance().emumAppState = EmumAppState.NOT_ACTIVE;
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (pausedInOnStop) {
            videoView.start();
            pausedInOnStop = false;
        }
    }

    @Override
    public void onBackPressed() {
        if(AdvInsTasker.getInsnance().pageStartAppBackVideoAct(getApplicationContext())){
            if(startAppAd != null) startAppAd.onBackPressed();
        }
        if (AdvInsTasker.getInsnance().pageAdmobBackVideoAct(getApplicationContext())){
            if(interstitial != null && interstitial.isLoaded()) interstitial.show();
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        Utils.getInstance().blnAppIsRun = false;
        if (AdvInsTasker.getInsnance().bannerAdmobVideoActivity(getApplicationContext())){
            if(adView != null) adView.destroy();
        }
        super.onDestroy();
    }

    private void savePreferences() {
        sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("saveState", videoLayout);
        editor.commit();
    }

    private void loadPreferences() {
        sharedPreferences = getPreferences(MODE_PRIVATE);
        videoLayout = sharedPreferences.getInt("saveState", 3);

        switch (videoLayout) {
            case 0:
                videoView.setScaleType(ScaleType.CENTER_INSIDE);
                break;
            case 1:
                videoView.setScaleType(ScaleType.CENTER);
                break;
            case 2:
                videoView.setScaleType(ScaleType.CENTER_CROP);
                break;
            case 3:
                videoView.setScaleType(ScaleType.FIT_CENTER);
                break;
            case 4:
                videoView.setScaleType(ScaleType.NONE);
                break;
        }
    }

}