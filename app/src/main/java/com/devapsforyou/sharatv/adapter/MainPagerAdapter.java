package com.devapsforyou.sharatv.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.devapsforyou.sharatv.R;
import com.devapsforyou.sharatv.fragments.AllFragment;

public class MainPagerAdapter extends FragmentPagerAdapter {

    public static final int NUM_ITEMS = 1;
    public static final int ALL_POS = 0;
    public static final int SHARED_POS = 1;

    private Context context;

    public MainPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case ALL_POS:
                return AllFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case ALL_POS:
                return context.getString(R.string.all);
            default:
                return "";
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

}
