package com.devapsforyou.sharatv.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.devapsforyou.sharatv.activity.PlayerActivity;
import com.devapsforyou.sharatv.instance.ChannelsIns;
import com.devapsforyou.sharatv.R;
import com.squareup.picasso.Picasso;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {

    private Context context;

    public NotesAdapter(Context context, int numNotes) {
        this.context = context;
    }

    @Override
    public NotesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_note, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String title = ChannelsIns.getInstance().getChannelBean(position).getTitle();
        String link = ChannelsIns.getInstance().getChannelBean(position).getLink();
        String imageLink = ChannelsIns.getInstance().getChannelBean(position).getImageLink();
        String showCast = ChannelsIns.getInstance().getChannelBean(position).getShowName();
        int color = getRandomColor(context);

        // Set text
        holder.titleTextView.setText(title);
        holder.noteTextView.setText(showCast);

        Picasso.with(context).load(context.getString(R.string.string_path_to_images) +  imageLink).into(holder.infoImageView);

        // Set visibilities
        holder.titleTextView.setVisibility(TextUtils.isEmpty(title) ? View.GONE : View.VISIBLE);
        holder.noteTextView.setVisibility(TextUtils.isEmpty(link) ? View.GONE : View.VISIBLE);

        // Set background color
        ((CardView) holder.itemView).setCardBackgroundColor(color);
    }

    private static int getRandomColor(Context context) {
        int[] colors;
        if (Math.random() >= 0.6) {
            colors = context.getResources().getIntArray(R.array.note_accent_colors);
        } else {
            colors = context.getResources().getIntArray(R.array.note_neutral_colors);
        }
        return colors[((int) (Math.random() * colors.length))];
    }

    @Override
    public int getItemCount() {
        return ChannelsIns.getInstance().getCountChannelsBeans();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView titleTextView;
        public TextView noteTextView;
        public ImageView infoImageView;
        private Context context;

        public ViewHolder(final View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context = itemView.getContext();
                    Intent intent = new Intent(context, PlayerActivity.class);
                    intent.putExtra(context.getString(R.string.intent), getAdapterPosition());
                    context.startActivity(intent);
                }
            });
            titleTextView = (TextView) itemView.findViewById(R.id.note_title);
            noteTextView = (TextView) itemView.findViewById(R.id.note_text);
            infoImageView = (ImageView) itemView.findViewById(R.id.note_info_image);
        }
    }

}
