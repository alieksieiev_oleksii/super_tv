package com.devapsforyou.sharatv.advertisement;

public class AdvInsStates {
    private static AdvInsStates instance;
    /**Toast message*/
    private String toast_message_main_act, toast_message_video_act;
    /**Dialog*/
    private boolean flag_show_dialog;
    private String dialog_title, dialog_message, dialog_link;
    /**Advertisement*/
    private String adv_banner_main_act, adv_banner_video_act, adv_page_start_app, adv_page_back_main_act, adv_page_back_video_act;
    /**Advertisement admob id's*/
    private String id_banner_main_act, id_banner_video_act, id_page_start_app, id_page_back_main_act, id_page_back_video_act; // Admob id's
    /**Advertisents flags to show*/
    private boolean flag_banner_main_act, flag_banner_video_act, flag_page_start_app, flag_page_back_main_act, flag_page_back_video_act;

    public void setToastMessage(String toast_message_main_act, String toast_message_video_act){
        this.toast_message_main_act = toast_message_main_act;
        this.toast_message_video_act = toast_message_video_act;
    }

    public void setAdvertisementIds(String _id_banner_main_act, String _id_banner_video_act, String _id_page_start_app, String _id_page_back_main_act, String _id_page_back_video_act){
        this.id_banner_main_act      = _id_banner_main_act;
        this.id_banner_video_act     = _id_banner_video_act;
        this.id_page_start_app       = _id_page_start_app;
        this.id_page_back_main_act   = _id_page_back_main_act;
        this.id_page_back_video_act  = _id_page_back_video_act;
    }

    public static AdvInsStates getInstance() {
        if (instance == null){
            instance = new AdvInsStates();
        }
        return instance;
    }

    public void setDialog(String _flag_show_dialog, String _dialog_title, String _dialog_message, String _dialog_link){
        this.flag_show_dialog = Boolean.parseBoolean(_flag_show_dialog);
        this.dialog_title = _dialog_title;
        this.dialog_message = _dialog_message;
        this.dialog_link = _dialog_link;
    }

    public void setAdvertisementInfo(String _adv_banner_main_act,     String _flag_banner_main_act,
                                     String _adv_banner_video_act,    String _flag_banner_video_act,
                                     String _adv_page_start_app,      String _flag_page_start_app,
                                     String _adv_page_back_main_act,  String _flag_page_back_main_act,
                                     String _adv_page_back_video_act, String _flag_page_back_video_act){

        this.adv_banner_main_act     = _adv_banner_main_act;
        this.adv_banner_video_act    = _adv_banner_video_act;
        this.adv_page_start_app      = _adv_page_start_app;
        this.adv_page_back_main_act  = _adv_page_back_main_act;
        this.adv_page_back_video_act = _adv_page_back_video_act;

        this.flag_banner_main_act     = Boolean.parseBoolean(_flag_banner_main_act);
        this.flag_banner_video_act    = Boolean.parseBoolean(_flag_banner_video_act);
        this.flag_page_start_app      = Boolean.parseBoolean(_flag_page_start_app);
        this.flag_page_back_main_act  = Boolean.parseBoolean(_flag_page_back_main_act);
        this.flag_page_back_video_act = Boolean.parseBoolean(_flag_page_back_video_act);
    }

    /***************************** getters toast massage *****************************/

    public String getToastMessageMainAct() {
        return toast_message_main_act;
    }

    public String getToastMessageVideoAct() {
        return toast_message_video_act;
    }

    /****************************** getters dialog ******************************/

    public boolean getFlagShowDialog() {
        return flag_show_dialog;
    }

    public String getDialogTitle() {
        return dialog_title;
    }

    public String getDialogMessage() {
        return dialog_message;
    }

    public String getDialogLink() {
        return dialog_link;
    }

    /****************************** getters advertisement ids ******************************/

    public String getIdBannerMainAct() {
        return id_banner_main_act;
    }

    public String getIdBannerVideoAct() {
        return id_banner_video_act;
    }

    public String getIdPageStartApp() {
        return id_page_start_app;
    }

    public String getIdPageBackMainAct() {
        return id_page_back_main_act;
    }

    public String getIdPageBackVideoAct() {
        return id_page_back_video_act;
    }

    /****************************** getters advertisement flags ******************************/

    public boolean getFlagBannerMainAct() {
        return flag_banner_main_act;
    }

    public boolean getFlagBannerVideoAct() {
        return flag_banner_video_act;
    }

    public boolean getFlagPageStartApp() {
        return flag_page_start_app;
    }

    public boolean getFlagPageBackMainAct() {
        return flag_page_back_main_act;
    }

    public boolean getFlagPageBackVideoAct() {
        return flag_page_back_video_act;
    }

    /****************************** getters advertisement names ******************************/

    public String getAdvBannerMainAct() {
        return adv_banner_main_act;
    }

    public String getAdvBannerVideoAct() {
        return adv_banner_video_act;
    }

    public String getAdvPageStartApp() {
        return adv_page_start_app;
    }

    public String getAdvPageBackMainAct() {
        return adv_page_back_main_act;
    }

    public String getAdvPageBackVideoAct() {
        return adv_page_back_video_act;
    }

}