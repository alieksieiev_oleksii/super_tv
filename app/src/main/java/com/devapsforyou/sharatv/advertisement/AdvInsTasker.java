package com.devapsforyou.sharatv.advertisement;

import android.content.Context;

import com.devapsforyou.sharatv.utils.Utils;
import com.devapsforyou.sharatv.R;

public class AdvInsTasker {

    private static AdvInsTasker advertisementTasker;

    public static AdvInsTasker getInsnance() {
        if(advertisementTasker == null){
            advertisementTasker = new AdvInsTasker();
        }
        return advertisementTasker;
    }

    public void toastMainActivity(Context context) {
        if ((AdvInsStates.getInstance().getToastMessageMainAct() != null) && (!AdvInsStates.getInstance().getToastMessageMainAct().isEmpty())){
            Utils.getInstance().showToast(context, AdvInsStates.getInstance().getToastMessageMainAct());
        }

    }

    public void toastVideoActivity(Context context) {
        if ((AdvInsStates.getInstance().getToastMessageVideoAct() != null) && (!AdvInsStates.getInstance().getToastMessageVideoAct().isEmpty())){
            Utils.getInstance().showToast(context, AdvInsStates.getInstance().getToastMessageVideoAct());
        }
    }

    public boolean bannerStartAppMainActivity(Context context) {
        if(AdvInsStates.getInstance().getFlagBannerMainAct()
                && AdvInsStates.getInstance().getAdvBannerMainAct() != null
                && AdvInsStates.getInstance().getAdvBannerMainAct().equals(context.getString(R.string.advertisement_startapp))){
            return true;
        } else {
            return false;
        }
    }

    public boolean bannerAdmobMainActivity(Context context) {
        if(AdvInsStates.getInstance().getFlagBannerMainAct()
                && AdvInsStates.getInstance().getAdvBannerMainAct() != null
                && AdvInsStates.getInstance().getAdvBannerMainAct().equals(context.getString(R.string.advertisement_admob))){
            return true;
        } else {
            return false;
        }
    }

    public boolean dialog() {
        if(AdvInsStates.getInstance().getFlagShowDialog()
                && AdvInsStates.getInstance().getDialogTitle() != null
                && AdvInsStates.getInstance().getDialogMessage() != null
                && AdvInsStates.getInstance().getDialogLink() != null){
            return true;
        } else {
            return false;
        }
    }

    public boolean pageAdmobBackMainAct(Context context) {
        if(AdvInsStates.getInstance().getFlagPageBackMainAct()
                && AdvInsStates.getInstance().getAdvPageBackMainAct() != null
                && AdvInsStates.getInstance().getAdvPageBackMainAct().equals(context.getString(R.string.advertisement_admob))){
            return true;
        } else {
            return false;
        }
    }

    public boolean pageStartAppBackMainAct(Context context) {
        if(AdvInsStates.getInstance().getFlagPageBackMainAct()
                && AdvInsStates.getInstance().getAdvPageBackMainAct() != null
                && AdvInsStates.getInstance().getAdvPageBackMainAct().equals(context.getString(R.string.advertisement_startapp))){
            return true;
        } else {
            return false;
        }
    }

    public boolean pageAdmobStartApplication(Context context) {
        if(AdvInsStates.getInstance().getFlagPageStartApp()
                && AdvInsStates.getInstance().getAdvPageStartApp() != null
                && AdvInsStates.getInstance().getAdvPageStartApp().equals(context.getString(R.string.advertisement_admob))){
            return true;
        } else {
            return false;
        }
    }

    public boolean pageStartAppStartApplication(Context context) {
        if(AdvInsStates.getInstance().getFlagPageStartApp()
                && AdvInsStates.getInstance().getAdvPageStartApp() != null
                && AdvInsStates.getInstance().getAdvPageStartApp().equals(context.getString(R.string.advertisement_startapp))){
            return true;
        } else {
            return false;
        }
    }

    /****************************** Video Activity ******************************/

    public boolean bannerAdmobVideoActivity(Context context) {
        if(AdvInsStates.getInstance().getFlagBannerVideoAct()
                && AdvInsStates.getInstance().getAdvBannerVideoAct() != null
                && AdvInsStates.getInstance().getAdvBannerVideoAct().equals(context.getString(R.string.advertisement_admob))){
            return true;
        } else {
            return false;
        }
    }

    public boolean bannerStartAppVideoActivity(Context context) {
        if(AdvInsStates.getInstance().getFlagBannerVideoAct()
                && AdvInsStates.getInstance().getAdvBannerVideoAct() != null
                && AdvInsStates.getInstance().getAdvBannerVideoAct().equals(context.getString(R.string.advertisement_startapp))){
            return true;
        } else {
            return false;
        }
    }

    public boolean pageAdmobBackVideoAct(Context context) {
        if(AdvInsStates.getInstance().getFlagPageBackVideoAct()
                && AdvInsStates.getInstance().getAdvPageBackVideoAct() != null
                && AdvInsStates.getInstance().getAdvPageBackVideoAct().equals(context.getString(R.string.advertisement_admob))){
            return true;
        } else {
            return false;
        }
    }

    public boolean pageStartAppBackVideoAct(Context context) {
        if(AdvInsStates.getInstance().getFlagPageBackVideoAct()
                && AdvInsStates.getInstance().getAdvPageBackVideoAct() != null
                && AdvInsStates.getInstance().getAdvPageBackVideoAct().equals(context.getString(R.string.advertisement_startapp))){
            return true;
        } else {
            return false;
        }
    }

}